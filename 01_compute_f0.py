## This codes aims to generate f0 in advance for reduce time spending during anonymization

import os
from os import path

import kaldiio
import numpy as np
from tqdm import tqdm

from anonymization.modules.vae.feat import compute_f0
from anonymization.modules.vae.util import HParams
from utils import get_datasets, parse_yaml, setup_logger

logger = setup_logger(__name__)


def generate_f0(datasets, hps):
    for i, (dataset_name, dataset_path) in enumerate(datasets.items()):
        logger.info(f'{i + 1}/{len(datasets)}: Processing dataset: {dataset_name} - {dataset_path}')

        output_dir = str(dataset_path) + '_f0'
        if not path.exists(output_dir):
            os.makedirs(output_dir, exist_ok=True)

        wav_scp = path.join(dataset_path, 'wav.scp')
        wav_scp_reader = kaldiio.load_scp_sequential(wav_scp)

        feat = hps.modules.feat

        for utt_id, (sr, sig) in tqdm(wav_scp_reader):
            wav = sig / np.abs(sig).max()
            f0 = compute_f0(
                waveform=wav,
                sample_rate=feat.sample_rate,
                win_length=feat.win_length, 
                hop_length=feat.hop_length,
                n_fft=feat.n_fft,
                norm=True
            )
        
            f0_file = path.join(output_dir, '%s.npy' % utt_id)
            with open(f0_file, 'wb') as f:
                np.save(f, f0)


if __name__ == "__main__":
    root_dir = os.getcwd()

    anon_conf_file = path.join(root_dir, 'configs', 'anon_vae.yaml')
    config = parse_yaml(anon_conf_file)
    datasets = get_datasets(config)

    hps = HParams(**config)

    generate_f0(datasets, hps)