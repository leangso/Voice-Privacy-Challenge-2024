## This codes aims to generate f0 in advance for reduce time spending during anonymization

import os
from os import path

import torch
import kaldiio
import numpy as np
from speechbrain.inference.speaker import EncoderClassifier
from tqdm import tqdm

from anonymization.modules.vae.util import HParams
from utils import get_datasets, parse_yaml, setup_logger

logger = setup_logger(__name__)


def generate_xvector(datasets, hps):
    ecapa = EncoderClassifier.from_hparams(source="speechbrain/spkrec-ecapa-voxceleb")


    for i, (dataset_name, dataset_path) in enumerate(datasets.items()):
        logger.info(f'{i + 1}/{len(datasets)}: Processing dataset: {dataset_name} - {dataset_path}')

        output_dir = str(dataset_path) + '_xv'
        if not path.exists(output_dir):
            os.makedirs(output_dir, exist_ok=True)

        wav_scp = path.join(dataset_path, 'wav.scp')
        wav_scp_reader = kaldiio.load_scp_sequential(wav_scp)

        for utt_id, (sr, sig) in tqdm(wav_scp_reader):
            wav = sig / np.abs(sig).max()

            wav = torch.FloatTensor(wav)
            xvector = ecapa.encode_batch(wav)
            xvector = xvector.squeeze(0).squeeze(0)

            xvector_file = path.join(output_dir, '%s.npy' % utt_id)
            with open(xvector_file, 'wb') as f:
                np.save(f, xvector)


if __name__ == "__main__":
    root_dir = os.getcwd()

    anon_conf_file = path.join(root_dir, 'configs', 'anon_vae.yaml')
    config = parse_yaml(anon_conf_file)
    datasets = get_datasets(config)

    hps = HParams(**config)

    generate_xvector(datasets, hps)