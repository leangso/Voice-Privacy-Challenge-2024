import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Conv1d, ConvTranspose1d
from torch.nn.utils import parametrizations, remove_weight_norm

######################################
######## ResidualConvEncoder  ########
######################################


#### Souce from: https://github.com/swasun/VQ-VAE-Speech

class Conv1DBuilder(object):

    @staticmethod
    def build(in_channels, out_channels, kernel_size, stride=1, padding=0, bias=True, kaiming_norm=False, weight_norm=False):
        conv = nn.Conv1d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            bias=bias,
        )  
        if weight_norm:
            conv = parametrizations.weight_norm(conv)
        elif kaiming_norm:
            nn.init.kaiming_normal_(conv.weight)

        return conv
    

class Residual(nn.Module):

    def __init__(self, in_channels, num_hiddens, num_residual_hiddens, kaiming_norm, weight_norm=False):
        super(Residual, self).__init__()
        
        relu_1 = nn.ReLU(True)
        conv_1 = Conv1DBuilder.build(
            in_channels=in_channels,
            out_channels=num_residual_hiddens,
            kernel_size=3,
            stride=1,
            padding=1,
            bias=False,
            kaiming_norm=kaiming_norm, 
            weight_norm=weight_norm
        )
       
        relu_2 = nn.ReLU(True)
        conv_2 = Conv1DBuilder.build(
            in_channels=num_residual_hiddens,
            out_channels=num_hiddens,
            kernel_size=1,
            stride=1,
            bias=False,
            kaiming_norm=kaiming_norm, 
            weight_norm=weight_norm
        )
       
        # All parameters same as specified in the paper
        self._block = nn.Sequential(
            relu_1,
            conv_1,
            relu_2,
            conv_2
        )
    
    def forward(self, x):
        return x + self._block(x)


class ResidualStack(nn.Module):

    def __init__(
            self, 
            in_channels, 
            num_hiddens, 
            num_residual_layers, 
            num_residual_hiddens, 
            kaiming_norm=False,
            weight_norm=False
        ):
        super(ResidualStack, self).__init__()
        
        self._num_residual_layers = num_residual_layers
        self._layers = nn.ModuleList(
            [Residual(in_channels, num_hiddens, num_residual_hiddens, kaiming_norm, weight_norm)] * self._num_residual_layers)
        
    def forward(self, x):
        for i in range(self._num_residual_layers):
            x = self._layers[i](x)
        return F.relu(x)
    

class ResidualConvEncoder(nn.Module):
    
    def __init__(
            self, 
            in_channel, 
            num_hiddens, 
            num_residual_layers, 
            num_residual_hiddens, 
            kaiming_norm=False,
            weight_norm=False
        ):

        super(ResidualConvEncoder, self).__init__()

        """
        2 preprocessing convolution layers with filter length 3
        and residual connections.
        """

        self._conv_1 = Conv1DBuilder.build(
            in_channels=in_channel,
            out_channels=num_hiddens,
            kernel_size=3,
            padding=1,
            kaiming_norm=kaiming_norm,
            weight_norm=weight_norm
        )
        
        self._conv_2 = Conv1DBuilder.build(
            in_channels=num_hiddens,
            out_channels=num_hiddens,
            kernel_size=3,
            padding=1,
            kaiming_norm=kaiming_norm,
            weight_norm=weight_norm
        )
       
        """
        1 strided convolution length reduction layer with filter
        length 4 and stride 2 (downsampling the signal by a factor
        of two).
        """
        self._conv_3 = Conv1DBuilder.build(
            in_channels=num_hiddens,
            out_channels=num_hiddens,
            kernel_size=4,
            stride=2, # timestep * 2
            padding=1,
            kaiming_norm=kaiming_norm,
            weight_norm=weight_norm
        )
      
        """
        2 convolutional layers with length 3 and
        residual connections.
        """

        self._conv_4 = Conv1DBuilder.build(
            in_channels=num_hiddens,
            out_channels=num_hiddens,
            kernel_size=3,
            padding=1,
            kaiming_norm=kaiming_norm,
            weight_norm=weight_norm
        )
       
        self._conv_5 = Conv1DBuilder.build(
            in_channels=num_hiddens,
            out_channels=num_hiddens,
            kernel_size=3,
            padding=1,
            kaiming_norm=kaiming_norm,
            weight_norm=weight_norm
        )

        """
        4 feedforward ReLu layers with residual connections.
        """

        self._residual_stack = ResidualStack(
            in_channels=num_hiddens,
            num_hiddens=num_hiddens,
            num_residual_layers=num_residual_layers,
            num_residual_hiddens=num_residual_hiddens,
            kaiming_norm=kaiming_norm,
            weight_norm=weight_norm
        )

    def forward(self, x):
        x_conv_1 = F.relu(self._conv_1(x))
        
        x = F.relu(self._conv_2(x_conv_1)) + x_conv_1
        
        x_conv_3 = F.relu(self._conv_3(x))
    
        x_conv_4 = F.relu(self._conv_4(x_conv_3)) + x_conv_3
    
        x_conv_5 = F.relu(self._conv_5(x_conv_4)) + x_conv_4
    
        y = self._residual_stack(x_conv_5) + x_conv_5
    
        return y
    

######################################
############## HiFi-GAN  #############
######################################


""""
    The codes are from: https://github.com/jaywalnut310/vits
"""

LRELU_SLOPE = 0.1


def init_weights(module, mean=0.0, std=0.01):
    classname = module.__class__.__name__
    if classname.find("Conv") != -1:
        module.weight.data.normal_(mean, std)


def get_padding(kernel_size, dilation=1):
    return int((kernel_size*dilation - dilation)/2)


class ResBlock1(torch.nn.Module):

    def __init__(self, channels, kernel_size=3, dilation=(1, 3, 5)):
        super(ResBlock1, self).__init__()
    
        self.convs1 = nn.ModuleList([
            parametrizations.weight_norm(Conv1d(channels, channels, kernel_size, 1, dilation=dilation[0], padding=get_padding(kernel_size, dilation[0]))),
            parametrizations.weight_norm(Conv1d(channels, channels, kernel_size, 1, dilation=dilation[1], padding=get_padding(kernel_size, dilation[1]))),
            parametrizations.weight_norm(Conv1d(channels, channels, kernel_size, 1, dilation=dilation[2], padding=get_padding(kernel_size, dilation[2])))
        ])
        self.convs1.apply(init_weights)

        self.convs2 = nn.ModuleList([
            parametrizations.weight_norm(Conv1d(channels, channels, kernel_size, 1, dilation=1, padding=get_padding(kernel_size, 1))),
            parametrizations.weight_norm(Conv1d(channels, channels, kernel_size, 1, dilation=1, padding=get_padding(kernel_size, 1))),
            parametrizations.weight_norm(Conv1d(channels, channels, kernel_size, 1, dilation=1, padding=get_padding(kernel_size, 1)))
        ])
        self.convs2.apply(init_weights)

    def forward(self, x, x_mask=None):
        for c1, c2 in zip(self.convs1, self.convs2):
            xt = F.leaky_relu(x, LRELU_SLOPE)
            if x_mask is not None:
                xt = xt * x_mask
            
            xt = c1(xt)
            xt = F.leaky_relu(xt, LRELU_SLOPE)
            if x_mask is not None:
                xt = xt * x_mask
            
            xt = c2(xt)
            x = xt + x
    
        if x_mask is not None:
            x = x * x_mask

        return x

    def remove_weight_norm(self):
        for l in self.convs1:
            remove_weight_norm(l)
        for l in self.convs2:
            remove_weight_norm(l)


class ResBlock2(torch.nn.Module):

    def __init__(self, channels, kernel_size=3, dilation=(1, 3)):
        super(ResBlock2, self).__init__()
    
        self.convs = nn.ModuleList([
            parametrizations.weight_norm(Conv1d(channels, channels, kernel_size, 1, dilation=dilation[0], padding=get_padding(kernel_size, dilation[0]))),
            parametrizations.weight_norm(Conv1d(channels, channels, kernel_size, 1, dilation=dilation[1], padding=get_padding(kernel_size, dilation[1])))
        ])
        self.convs.apply(init_weights)

    def forward(self, x, x_mask=None):
        for c in self.convs:
            xt = F.leaky_relu(x, LRELU_SLOPE)
            if x_mask is not None:
                xt = xt * x_mask
        
            xt = c(xt)
            x = xt + x
    
        if x_mask is not None:
            x = x * x_mask

        return x

    def remove_weight_norm(self):
        for l in self.convs:
            remove_weight_norm(l)


class Generator(torch.nn.Module):

    def __init__(self,
                 in_channels,
                 resblock_type,
                 resblock_kernel_sizes,
                 resblock_dilation_sizes,
                 upsample_rates,
                 upsample_initial_channel,
                 upsample_kernel_sizes,
                 gin_channels=0):
        super(Generator, self).__init__()

        self.num_kernels = len(resblock_kernel_sizes)
        self.num_upsamples = len(upsample_rates)

        self.conv_pre = Conv1d(in_channels, upsample_initial_channel, 7, 1, padding=3)

        resblock = ResBlock1 if resblock_type == '1' else ResBlock2

        self.ups = nn.ModuleList()
        for i, (u, k) in enumerate(zip(upsample_rates, upsample_kernel_sizes)):
            self.ups.append(parametrizations.weight_norm(
                ConvTranspose1d(upsample_initial_channel//(2**i), upsample_initial_channel//(2**(i+1)), k, u, padding=(k-u)//2)))

        self.resblocks = nn.ModuleList()
        for i in range(len(self.ups)):
            ch = upsample_initial_channel//(2**(i+1))
            for j, (k, d) in enumerate(zip(resblock_kernel_sizes, resblock_dilation_sizes)):
                self.resblocks.append(resblock(ch, k, d))

        self.conv_post = Conv1d(ch, 1, 7, 1, padding=3, bias=False)
    
        self.ups.apply(init_weights)

        if gin_channels != 0:
            self.cond = nn.Conv1d(gin_channels, upsample_initial_channel, 1)

    def forward(self, x, g=None):
        x = self.conv_pre(x)
        if g is not None:
            x = x + self.cond(g)

        for i in range(self.num_upsamples):
            x = F.leaky_relu(x, LRELU_SLOPE)
            x = self.ups[i](x)
            xs = None
            for j in range(self.num_kernels):
                if xs is None:
                    xs = self.resblocks[i*self.num_kernels+j](x)
                else:
                    xs += self.resblocks[i*self.num_kernels+j](x)
    
            x = xs / self.num_kernels

        x = F.leaky_relu(x)
        x = self.conv_post(x)
        x = torch.tanh(x)

        return x

    def remove_weight_norm(self):
        for l in self.ups:
            remove_weight_norm(l)
        for l in self.resblocks:
            remove_weight_norm(l)