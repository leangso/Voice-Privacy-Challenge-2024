import amfm_decompy.basic_tools as basic
import amfm_decompy.pYAAPT as pYAAPT
import numpy as np
import torch
import torch.nn.functional as F
from torchaudio import transforms


def dynamic_range_compression(x, C=1, clip_val=1e-5):
    """
    PARAMS
    ------
    C: compression factor
    """
    return torch.log(torch.clamp(x, min=clip_val) * C)


def spectrogram(waveform, win_length=400, hop_length=160, n_fft=512, power=1, center=False):
    audio2spec = transforms.Spectrogram(
        win_length=win_length, 
        hop_length=hop_length, 
        n_fft=n_fft,
        power=power,
        center=center
    )
    waveform = F.pad(waveform.unsqueeze(1), (int((n_fft-hop_length)/2), int((n_fft-hop_length)/2)), mode='reflect').squeeze(1)
    return audio2spec(waveform)


def melspectrogram(
        waveform, 
        sample_rate=16000, 
        win_length=400, 
        hop_length=160, 
        n_fft=512,
        n_mel=40, 
        f_min=0, 
        f_max=None,
        power=1,
        center=False):

    audio2mel = transforms.MelSpectrogram(
        sample_rate=sample_rate, 
        win_length=win_length, 
        hop_length=hop_length, 
        n_fft=n_fft,
        power=power,
        n_mels=n_mel,
        f_min=f_min, 
        f_max=f_max,
        center=center
    )
    waveform = F.pad(waveform.unsqueeze(1), (int((n_fft-hop_length)/2), int((n_fft-hop_length)/2)), mode='reflect').squeeze(1)
    mel = audio2mel(waveform)
    mel = dynamic_range_compression(mel)
    return mel


def compute_f0(waveform, sample_rate=16000, win_length=400, hop_length=160, n_fft=512, norm=False):
    waveform = np.pad(waveform, int((n_fft - hop_length) / 2), mode='reflect')
    signal = basic.SignalObj(data=waveform, fs=sample_rate)

    pitch = pYAAPT.yaapt(
        signal, 
        **{ 'frame_length' : (win_length / sample_rate) * 1000, 'frame_space' : (hop_length / sample_rate) * 1000})
    
    f0 = pitch.samp_values
    if norm is True:
        f0 = np.log(np.where(f0 == 0., np.ones_like(f0), f0))

    return f0


def compute_energy(waveform, win_length=400, hop_length=160, n_fft=512, norm=False):
    spec = spectrogram(waveform, win_length, hop_length, n_fft)
    power_spec = torch.square(spec)

    energy = torch.sqrt(torch.clamp(power_spec.sum(dim=1), min=1.0e-10))
    if norm is True:
        energy = energy / (energy * (energy != 0.0)).mean(dim=1).unsqueeze(1) # normalize
    
    return energy