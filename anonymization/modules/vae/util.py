import os

import torch


def scan_dir(path, type=str):
    dirs = list(os.scandir(path))
    dirs.sort(key=lambda x: type(x.name))
    return dirs


def merge_dict(to_dct, from_dct):
    for k, v in iter(from_dct.items()):
        if k in to_dct and isinstance(to_dct[k], dict) and isinstance(from_dct[k], dict):
            merge_dict(to_dct[k], from_dct[k])
        else:
            to_dct[k] = from_dct[k]


def load_ckpt(cls, ckpt_file, hparams=None):
    ckpt = torch.load(ckpt_file, map_location=torch.device('cpu'))

    params = ckpt.get('hparams')
    if hparams is not None:
        merge_dict(params, hparams)

    model = cls(**params)
    model.load_state_dict(ckpt.get('state_dict'))
    return model


class HParams():

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            if type(v) == dict:
                v = HParams(**v)
            self[k] = v

    def keys(self):
        return self.__dict__.keys()

    def items(self):
        return self.__dict__.items()

    def values(self):
        return self.__dict__.values()

    def __len__(self):
        return len(self.__dict__)

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        return setattr(self, key, value)

    def __contains__(self, key):
        return key in self.__dict__

    def __repr__(self):
        return self.__dict__.__repr__()
