import os
from typing import Union

from torch.utils.data import Dataset

""""
The codes are from anonymization/pipelines/nac/data.py
"""
class SCPPathDataset(Dataset):
    
    def __init__(
            self,
            scp_file: str,
            root: Union[str, None] = None,
            size: int = -1
    ):
        """
        Literally just takes a Kaldi-format scp file (damn you Kaldi!) and returns utterance ids and paths.
        Optionally returns basename. Optionally appends root to the path.
        Don't use this with scp files that have fancy piping, it will not work.

        @param scp_file: path to scp file
        @param root: root to append to the paths read form scp. If None, will not be appended.
        """
        self.root = root

        f0_dir = os.path.dirname(scp_file) + '_f0'
        xv_dir = os.path.dirname(scp_file) + '_xv'

        # first get the available data
        with open(scp_file, 'r') as f:
            scp_lines = f.readlines()

        self.data = []
        for idx, line in enumerate(scp_lines):
            if size != -1 and idx >= size:
                break

            parsed_line = line.strip().split()
            if len(parsed_line) == 2:
                utt_id, audio_file = parsed_line
            else:
                #this is the case of train-360
                utt_id, _, _, _, _, audio_file, _ = parsed_line
    
            filename = os.path.basename(audio_file)
            if root:
                audio_file = os.path.join(root, audio_file)

            f0_filename = filename.replace('.wav', '.npy').replace('.flac', '.npy')
            f0_file = os.path.join(f0_dir, f0_filename)

            xv_filename = filename.replace('.wav', '.npy').replace('.flac', '.npy')
            xv_file = os.path.join(xv_dir, xv_filename)


            row = (utt_id, audio_file, filename, f0_file, xv_file)

            self.data.append(row)

    def __getitem__(self, idx):
        utt_id, audio_file, filename, f0_file, xv_file = self.data[idx]
        return utt_id, audio_file, filename, f0_file, xv_file

    def __len__(self):
        return len(self.data)
    

# to provide extra parameters during anonymization
class DataCollate():

    def __init__(self, hps):
        self.hps = hps

    def __call__(self, batch):
        utt_id, audio_file, filename, f0_file, xv_file = list(zip(*batch))
        return utt_id, audio_file, filename, f0_file, xv_file, self.hps