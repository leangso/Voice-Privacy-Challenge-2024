import random

import numpy as np
import torch
import torch.nn as nn
import torchaudio
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import normalize
# from speechbrain.inference.speaker import EncoderClassifier

from anonymization.modules.vae.feat import (compute_energy, compute_f0, melspectrogram)
from anonymization.modules.vae.model import (Decoder, Encoder, ProsodyEncoder, Quantizer)
from anonymization.modules.vae.util import load_ckpt, scan_dir


def normalize_pt(tensor, order=2, dim=-1):
    """
    Normalize a tensor using L2 norm by default across a specified dimension.
    
    Args:
    tensor (torch.Tensor): The input tensor to normalize.
    order (int): The order of the norm. Default is 2 (L2 norm).
    dim (int): The dimension to compute the norm over. Default is -1 (last dimension).
    
    Returns:
    torch.Tensor: The normalized tensor.
    """
    norm = torch.linalg.norm(tensor, ord=order, dim=dim, keepdim=True)
    normalized_tensor = tensor / (norm + 1e-10)  # Added epsilon to avoid division by zero
    return normalized_tensor


def read_spk_infos(spk_info_file, datasets=['train-clean-100', 'train-other-500']):
    spk_infos = {}
    with open(spk_info_file, 'r') as reader:
        for idx, line in enumerate(reader):
            if idx < 12: # skip description
                continue

            record = line.strip().split('|') # READER|GENDER|SUBSET NAME|MINUTES|NAME
            record = [e.strip() for e in record if e != '']

            if record[2] not in datasets:
                continue

            record.append(idx - 1) # add spk_id

            spk_code = record[0]
            spk_infos[spk_code] = record

    return spk_infos

def read_f0_mean(f0_mean_file, datasets=['train-clean-100', 'train-other-500']):
    f0_mean = {}
    with open(f0_mean_file, 'r') as reader:
        for line in reader:
            record = line.strip().split('|') # spk_code|f0_mean
            f0_mean[record[0]] = float(record[1])

    return f0_mean

def random_alter(value, rand=None, low=0.8, high=1.2):
    if rand is None:
        # interval_choice = torch.randint(0, 2, (1,)).item()

        # if interval_choice == 0:
        #     high = 0.9
        # else:
        #     low = 1.1

        rand = (high - low) * torch.rand(1).item() + low

        while rand == 1.0:
            rand = (high - low) * torch.rand(1).item() + low

    value = value * rand
    # value = value * 0.1
 
    return value, rand

class Anonymizer(nn.Module):

    def __init__(self, hps):
        super(Anonymizer, self).__init__()

        self.hps = hps
        
        # model
        # self.ecapa = EncoderClassifier.from_hparams(source=hps.ecapa, run_opts={'device': 'cuda'})
        self.encoder = load_ckpt(Encoder, hps.encoder)
        self.quantizer = load_ckpt(Quantizer, hps.quantizer)

        if hps.prosody is not None:
            self.prosody = load_ckpt(ProsodyEncoder, hps.prosody)
            self.prosody.eval()

        self.decoder = load_ckpt(Decoder, hps.decoder)

        self.encoder.eval()
        self.quantizer.eval()
        self.decoder.eval()

        xvector_pool, f0_mean_pool = self.load_spk_pool()
        self.xvector_pool = xvector_pool
        self.f0_mean_pool = f0_mean_pool
       
    def load_spk_pool(self):
        # read trained speaker infos: use to filters in xvector dir
        spk_infos = read_spk_infos(self.hps.anno.spk_info_file)

        f0_means = read_f0_mean(self.hps.anno.f0_mean_file)

        xvector_pool = []
        f0_mean_pool = []
        for xvector_file in scan_dir(self.hps.anno.xvector_dir):
            if not xvector_file.is_file():
                continue
            
            spk_code = xvector_file.name.replace('.npy', '')
            if spk_infos.get(spk_code) is None:
                continue

            xvector = np.load(xvector_file)
            f0_mean = f0_means[spk_code]

            xvector_pool.append(xvector)
            f0_mean_pool.append(f0_mean)

        return xvector_pool, f0_mean_pool
    
    def preprocess(self, wav, f0_file=None, xv_file=None, device='cpu'):
        mel = melspectrogram(
            wav, 
            sample_rate=self.hps.mel.sample_rate,
            win_length=self.hps.mel.win_length, 
            hop_length=self.hps.mel.hop_length,
            n_fft=self.hps.mel.n_fft,
            n_mel=80
        )

        if self.hps.anno.use_external_f0 is True and f0_file is not None:
            f0 = np.load(f0_file)
            # recover f0 from apply log
            f0 = np.exp(f0)
            f0 = np.where(f0 == 1., 0., f0)
        else:
            f0 = compute_f0(
                wav.squeeze(0).numpy(),
                sample_rate=self.hps.mel.sample_rate,
                win_length=self.hps.mel.win_length, 
                hop_length=self.hps.mel.hop_length,
                n_fft=self.hps.mel.n_fft,
                norm=False
            )

        f0 = torch.FloatTensor(f0).unsqueeze(0)

        energy = compute_energy(
            wav, 
            win_length=self.hps.mel.win_length, 
            hop_length=self.hps.mel.hop_length,
            n_fft=self.hps.mel.n_fft,
            norm=False
        )

        if self.hps.anno.use_external_xvector is True:
            xvector = np.load(xv_file)
            xvector = torch.FloatTensor(xvector).unsqueeze(0)
        else:
            xvector = self.ecapa.encode_batch(wav.to(device))
            xvector = xvector.squeeze(0)

        # xvector = normalize_pt(xvector)

        f0 = f0[:, :mel.size(2)]

        # mel       : 1, C, L
        # f0        : 1, L
        # energy    : 1, L
        # xvector   : 1, 192
        return mel, f0, energy, xvector
    
    def compute_pseudo_xvector(self, xvector):
        xvectors = np.array(self.xvector_pool)

        xvectors = normalize(xvectors) if self.hps.anno.norm_xvector is True else xvectors
        xvector = normalize(xvector.reshape(1, -1)) if self.hps.anno.norm_xvector is True else xvector.reshape(1, -1)

        cosine = cosine_similarity(xvector, xvectors).flatten()
        indices = np.argsort(cosine)
        indices = indices[:self.hps.anno.diff_spk] # N farthest speakers
        indices = np.random.choice(indices, size=self.hps.anno.diff_spk_sel, replace=False) # N* farthest speakers

        diff_spk = np.array(self.xvector_pool)[indices]
        diff_spk_mean = np.mean(diff_spk, axis=0)
        most_dff_xvector = diff_spk[0]

        if self.hps.anno.method == 'm1':
            pseudo_vector = most_dff_xvector
        elif self.hps.anno.method == 'm2':
            pseudo_vector = diff_spk_mean
        elif self.hps.anno.method == 'm3':
            pseudo_vector = most_dff_xvector + diff_spk_mean
        elif self.hps.anno.method == 'm4':
            pseudo_vector = np.floor(most_dff_xvector) + diff_spk_mean
        elif self.hps.anno.method == 'm5':
            mu = 0
            sigma = 1
            gaussian = mu + sigma * np.random.randn(*most_dff_xvector.shape)
            pseudo_vector = most_dff_xvector + gaussian
        else:
            raise Exception(f'Not support anonymization method: {self.hps.anno.method}')

        return pseudo_vector, indices

    def anonymize(self, batch, device='cpu'):
        utt_ids, wav_files, basenames, f0_files, xv_files, hps = batch

        # prepare wavs
        wavs = [torchaudio.load(wav_file, normalize=True)[0] for wav_file in wav_files]

        wavs_len = [x.size(1) for x in wavs]
        wavs_padded = torch.FloatTensor(len(wavs), 1, max(wavs_len))
        wavs_padded.zero_()

        for i, wav in enumerate(wavs):
           wavs_padded[i, :, :wav.size(1)] = wav

        # prepare features
        inputs = []
        for i, wav in enumerate(wavs):
            f0_file = f0_files[i] if f0_files is not None else None
            xv_file = xv_files[i] if xv_files is not None else None
            input = self.preprocess(wav, f0_file, xv_file, device)

            inputs.append(input)
        
        mels, f0s, energies, xvectors = list(zip(*inputs))

        # anonymize features
        pseudo_xvs = []
        pseudo_f0s = []
        pseudo_eneries = []
        for i, xv in enumerate(xvectors):
            # pseudo vector
            pseudo_xv, indices = self.compute_pseudo_xvector(xv.cpu().numpy())
            pseudo_xvs.append(torch.FloatTensor(pseudo_xv))

            # pseudo f0
            if self.hps.anno.f0_method is None:
               pseudo_f0 = f0s[i]
            elif self.hps.anno.f0_method == 'p1':
                pseudo_f0, rnd = random_alter(f0s[i])
            elif self.hps.anno.f0_method == 'p2':
                f0_means = np.array(self.f0_mean_pool)[indices]
                f0_mean = np.mean(f0_means)
                pseudo_f0 = f0s[i] / (f0_mean + 1e-10) 
            else:
                raise Exception(f'Prosody method not support: {self.hps.anno.prosody_method}')
            
            pseudo_f0 = torch.where(pseudo_f0 == 0, torch.ones_like(pseudo_f0), pseudo_f0)
            pseudo_f0 = torch.log(pseudo_f0)
            pseudo_f0s.append(pseudo_f0)

            # pseudo energy
            energy = energies[i]

            if self.hps.anno.energy_method == 'p1':
                if rnd is not None:
                    rnd_ = rnd / 2
                energy, rnd = random_alter(energy, rand=rnd_)
            
            energy = energy / (energy * (energy != 0.0)).mean(dim=1).unsqueeze(1)

            pseudo_eneries.append(energy)

        batch_size = len(mels)
        max_len = max([x.size(2) for x in mels])

        mel_padded = torch.FloatTensor(batch_size, mels[0].size(1), max_len)
        f0_padded = torch.FloatTensor(batch_size, 1, max_len)
        energy_padded = torch.FloatTensor(batch_size, 1, max_len)
        mel_padded.zero_()
        f0_padded.zero_()
        energy_padded.zero_()

        for i in range(batch_size):
            mel = mels[i]
            f0 = pseudo_f0s[i]
            energy = pseudo_eneries[i]

            mel_padded[i, :, :mel.size(2)] = mel
            f0_padded[i, :, :f0.size(1)] = f0
            energy_padded[i, :, :energy.size(1)] = energy
    
        mel_padded = mel_padded.to(device)
        f0_padded = f0_padded.to(device)
        energy_padded = energy_padded.to(device)

        # anonymization
        with torch.no_grad(): 
            z = self.encoder(mel_padded)
            e = self.quantizer.encode(z)

            pseudo_xv = torch.stack(pseudo_xvs).to(device)

            if self.hps.prosody is not None:
                c = torch.cat((f0_padded, energy_padded), dim=1)
                c = self.prosody(c)
            else:
                c = None

            anon_wav = self.decoder(e, c=c, g=pseudo_xv)

        # prepare anonymized wavs
        out_wavs = []
        for i, wav in enumerate(anon_wav.detach().cpu().numpy()):
            out_wavs.append(wav[:wavs_len[i]])

        return out_wavs