import torch
import torch.nn as nn
import torch.nn.functional as F
from anonymization.modules.vae.module import Conv1DBuilder, ResidualConvEncoder
from anonymization.modules.vae.module import Generator
from vector_quantize_pytorch import VectorQuantize


class Encoder(nn.Module):

    def __init__(
            self, 
            in_channel=80, 
            out_channel=256,
            hidden_channel=768, 
            residual_layer=4, 
            residual_hidden=768, **kwargs):
        
        super(Encoder, self).__init__()
        
        self.encoder = ResidualConvEncoder(
            in_channel=in_channel, 
            num_hiddens=hidden_channel, 
            num_residual_layers=residual_layer, 
            num_residual_hiddens=residual_hidden,
            kaiming_norm=False,
            weight_norm=False
        )
       
        self.post_conv = Conv1DBuilder.build(hidden_channel, out_channel, 1, 1, kaiming_norm=False, weight_norm=False)

    def forward(self, x):
        """"
            Args:
                x           : B, C, L

            Outputs:
                z           : B, C, L
        """
        z = self.encoder(x) # B, C, L
        z = self.post_conv(z)

        return z


class Quantizer(nn.Module):

    def __init__(
            self, 
            in_channel=256, 
            num_quantizer=8,
            codebook_size=512, 
            codebook_dim=256, 
            shared_codebook=False,
            kmeans_init=False,
            kmeans_iters=1,
            threshold_ema_dead_code=0,
            commitment_cost=0.25, 
            decay=0.99,
            epsilon=0.00001, **kwargs):
        
        super(Quantizer, self).__init__()

        self.quantizer = VectorQuantize(
            dim=in_channel,
            codebook_size=codebook_size,     
            codebook_dim=codebook_dim,
            heads=num_quantizer,
            separate_codebook_per_head=not shared_codebook,
            kmeans_init=kmeans_init,
            kmeans_iters=kmeans_iters,
            threshold_ema_dead_code=threshold_ema_dead_code,
            commitment_weight=commitment_cost,
            decay=decay,             
            eps=epsilon
        )

        self.codebook_size = codebook_size
        self.num_quantizer = num_quantizer
        self.shared_codebook = shared_codebook

    def forward(self, x):
        """"
            Args:
                x           : B, C, L

            Outputs:
                z           : B, Q, L
                vq_loss     :
                indice      :
                perplexity  :
        """
        z, indice, vq_loss = self.quantizer(x.transpose(1, 2)) # B, L, Q
        z = z.transpose(1, 2) # B, Q, L

        # total perplexity
        num_code = self.codebook_size if self.shared_codebook else self.num_quantizer * self.codebook_size
        encoding = F.one_hot(indice.reshape(-1), num_code).float()
        avg_prob = torch.mean(encoding, dim=0)
        perplexity = torch.exp(-torch.sum(avg_prob * torch.log(avg_prob + 1e-10)))

        return z, vq_loss, indice, perplexity

    def encode(self, x):
        """"
            Args:
                x           : B, C, L

            Outputs:
                z           : B, Q, L
        """
        z, _, _ = self.quantizer(x.transpose(1, 2)) # B, L, Q
        z = z.transpose(1, 2) # B, Q, L

        return z


class ProsodyEncoder(nn.Module):

    def __init__(
            self, 
            in_channel=2, 
            hidden_channel=128,
            out_channel=256, **kwargs):
        
        super(ProsodyEncoder, self).__init__()

        self.rnn = nn.GRU(
            in_channel, 
            hidden_channel,
            num_layers=1, 
            batch_first=True, 
            bidirectional=True
        )
        self.fc = nn.Linear(2*hidden_channel, out_channel)

    def forward(self, x):
        """"
            Args:
                x       : B, C, L

        """
        p, _ = self.rnn(x.transpose(1, 2))
        p = self.fc(p)
        p = p.transpose(1, 2)
        return p


##############################################


class Decoder(nn.Module):

    def __init__(
            self, 
            in_channel=64, 
            resblock_type='1',
            resblock_kernel_sizes=[3, 7, 11],
            resblock_dilation_sizes=[[1, 3, 5], [1, 3, 5], [1, 3, 5]],
            upsample_factors=[10, 4, 4],
            upsample_initial_channel=512,
            upsample_kernel_sizes=[20, 8, 8],
            c_channel=0,
            c_rnn_channel=0,
            g_channel=0,
            g_num_spk=0):
        
        super(Decoder, self).__init__()

        if c_rnn_channel > 0:
            self.rnn = nn.GRU(
                in_channel + c_channel, 
                c_rnn_channel,
                num_layers=2, 
                batch_first=True, 
                bidirectional=True
            )

        self.generator = Generator(
            in_channels=(in_channel + c_channel) if c_rnn_channel == 0 else 2 * c_rnn_channel,
            resblock_type=resblock_type,
            resblock_kernel_sizes=resblock_kernel_sizes,
            resblock_dilation_sizes=resblock_dilation_sizes,
            upsample_rates=upsample_factors,        
            upsample_initial_channel=upsample_initial_channel,
            upsample_kernel_sizes=upsample_kernel_sizes,
            gin_channels=g_channel
        )

        if g_num_spk > 1:
            self.g_emb = nn.Embedding(g_num_spk, g_channel)

        self.g_num_spk = g_num_spk
        self.c_channel = c_channel
        self.c_rnn_channel = c_rnn_channel

    def forward(self, z, c=None, g=None):
        """"
            Args:
                z       : B, Q, L
                c       : B, Q, L       - local conditioning
                g       : B, E | ID     - global conditioning
    
            Outputs:
                y_hat   : B, T
        """
        z = F.interpolate(z, scale_factor=2)

        if c is not None:
            if c.size(2) > z.size(2):
                c = c[:, :, :z.size(2)]
    
            z = torch.cat((z, c), dim=1)

        if  self.c_rnn_channel > 0:
            z, _ = self.rnn(z.transpose(1, 2))
            z = z.transpose(1, 2)

        if g is not None:
            if self.g_num_spk > 1:
                g = self.g_emb(g)

            g = g.unsqueeze(2)
            g = g.expand(-1, -1, z.shape[2])

        y_hat = self.generator(z, g)
        y_hat = y_hat.squeeze(1)
        return y_hat