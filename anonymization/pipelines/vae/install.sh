#!/bin/bash

# Fresh install with "rm .micromamba/micromamba .done-*"

set -e

nj=$(nproc)
home=$PWD
venv_dir=$PWD/venv
source ./env.sh

compute_and_write_hash "anonymization/pipelines/vae/requirements.txt"  # SHA256: fc2567e5b4f69c90186e43ab6719fd9dd30b044541f913852d69e19af96d6a56
trigger_new_install ".done-vae-requirements"

mark=.done-vae-requirements
if [ ! -f $mark ]; then
  echo " == Installing VAE python libraries =="
  pip3 install -r anonymization/pipelines/vae/requirements.txt  || exit 1
  touch $mark
fi
