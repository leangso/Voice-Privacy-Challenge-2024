import os
import soundfile as sf
from lightning.pytorch import LightningModule, Trainer
from torch.utils.data import DataLoader
from pathlib import Path

from anonymization.modules.vae.anonymizer import Anonymizer
from anonymization.modules.vae.data import DataCollate, SCPPathDataset
from anonymization.modules.vae.util import HParams
from anonymization.pipelines.pipeline import Pipeline
from utils import (create_clean_dir, remove_contents_in_dir, setup_logger)

logger = setup_logger(__name__)

# module for working with lighthing
class AnonymizerLM(LightningModule):

    def __init__(self, hps):
        super(AnonymizerLM, self).__init__()

        self.hps = hps
        self.model = Anonymizer(hps)

    def predict_step(self, batch, batch_idx, dataloader_idx=0):
        anon_wavs = self.model.anonymize(batch, self.device)
        return anon_wavs
    
    def on_predict_batch_end(self, anon_wavs, batch, batch_idx, dataloader_idx=0):
        utt_ids, audio_files, filenames, f0_files, xv_files, hps = batch

        # write results to files
        for i, anon_wav in enumerate(anon_wavs):
            audio_file = os.path.join(hps.results_dir, filenames[i])
            audio_file = audio_file.replace('.flac', '.wav')

            if os.path.exists(audio_file):
                print(f'Audio file exist: {audio_file}')
            
            sf.write(audio_file, anon_wav, format='wav', samplerate=16000)


class VAEPipeline(Pipeline):

    def __init__(self, config: dict, force_compute: bool = False, devices: list = [0]):
        """
        Args:
            config: dictionary of various configurations.
            force_compute: force to re-anonymize the dataset
            devices: unused, it's just there for API consistency for now.
        """
        hps = HParams(**config)
    
        self.anonymizer = AnonymizerLM(hps.modules)

        self.trainer = Trainer(devices=hps.modules.devices, accelerator='gpu', strategy='auto')

        self.hps = hps
        self.force_compute = force_compute

    def run_anonymization_pipeline(self, datasets):
        for i, (dataset_name, dataset_path) in enumerate(datasets.items()):
            logger.info(f"{i + 1}/{len(datasets)}: VAE Processing {dataset_name}...")

            # init output dir
            output_path = Path(str(dataset_path) + self.hps.anon_suffix) # dataset output dir
            if output_path.exists():
                if self.force_compute is True:
                    logger.info(f"Clean directory: {output_path}")
                    remove_contents_in_dir(output_path)
                else:
                    logger.info(f"Existed data => Skip {dataset_name}")
                    continue

            results_dir = output_path / self.hps.results_dir  # wav dir
            create_clean_dir(results_dir, force=self.force_compute)

            # prepare dataset
            logger.info(f'Init dataset: {dataset_path}')

            dataset = SCPPathDataset(
                scp_file=os.path.join(dataset_path, "wav.scp"),
                root='.',  # this assumes that the method is always called by the main script
                size=self.hps.modules.anno.size
            )

            hps = HParams(**{
                # 'dataset_path': dataset_path,
                'results_dir': results_dir
            })

            data_collate = DataCollate(hps)
            dataloader = DataLoader(dataset, batch_size=self.hps.modules.anno.batch_size, num_workers=10, collate_fn=data_collate)

            # anonymize
            logger.info(f'Start anonymization: dataset={dataset_path}')

            self.trainer.predict(self.anonymizer, dataloaders=dataloader, return_predictions=False)
        
            logger.info(f'Done: dataset={dataset_path}')