
# aker
exp_name=exp049
exp_dir=/home/m-psi/leangso/work/result/vpc/$exp_name
version='epoch=139-step=101080'
server=leangso@aker.imag.fr

# nlp server
# exp_name=exp039
# exp_dir=/home/sotheara.leang/phd/exp/result-vpc/$exp_name
# version='epoch=5-step=8658'
# server=sotheara.leang@103.16.63.233

# puck server
# exp_name=exp048
# exp_dir=/data/Sotheara/exp/result-vpc/$exp_name
# version='epoch=9-step=7220'
# server=leangso@puck.imag.fr

out_dir=/home/leangso/work/Voice-Privacy-Challenge-2024/exp/vae/$exp_name

mkdir -p $out_dir 
mkdir -p $out_dir/ckpt

scp -r ${server}:$exp_dir/tboard $out_dir || exit 1

# scp ${server}:$exp_dir/ckpt/encoder-${version}.ckpt ${out_dir}/ckpt \
#     && scp ${server}:$exp_dir/ckpt/quantizer-${version}.ckpt ${out_dir}/ckpt \
#     && scp ${server}:$exp_dir/ckpt/prosody-encoder-${version}.ckpt ${out_dir}/ckpt \
#     && scp ${server}:$exp_dir/ckpt/decoder-${version}.ckpt ${out_dir}/ckpt || exit 1

# nlp server
# scp -P 2250 -i '~/work/ssh/cadt' -r ${server}:$exp_dir/tboard $out_dir || exit 1

# scp -P 2250 -i '~/work/ssh/cadt' ${server}:$exp_dir/ckpt/encoder-${version}.ckpt ${out_dir}/ckpt \
#     && scp -P 2250 -i '~/work/ssh/cadt' ${server}:$exp_dir/ckpt/quantizer-${version}.ckpt ${out_dir}/ckpt \
#     && scp -P 2250 -i '~/work/ssh/cadt' ${server}:$exp_dir/ckpt/prosody-encoder-${version}.ckpt ${out_dir}/ckpt \
#     && scp -P 2250 -i '~/work/ssh/cadt' ${server}:$exp_dir/ckpt/decoder-${version}.ckpt ${out_dir}/ckpt || exit 1

# puck server    
# scp -i '~/work/ssh/lig_rsa' -r ${server}:$exp_dir/tboard $out_dir || exit 1

# scp -i '~/work/ssh/lig_rsa' ${server}:$exp_dir/ckpt/encoder-${version}.ckpt ${out_dir}/ckpt \
#     && scp -i '~/work/ssh/lig_rsa' ${server}:$exp_dir/ckpt/quantizer-${version}.ckpt ${out_dir}/ckpt \
#     && scp -i '~/work/ssh/lig_rsa' ${server}:$exp_dir/ckpt/prosody-encoder-${version}.ckpt ${out_dir}/ckpt \
#     && scp -i '~/work/ssh/lig_rsa' ${server}:$exp_dir/ckpt/decoder-${version}.ckpt ${out_dir}/ckpt || exit 1
